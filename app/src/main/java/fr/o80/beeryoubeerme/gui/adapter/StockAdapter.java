package fr.o80.beeryoubeerme.gui.adapter;

import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.o80.beeryoubeerme.R;
import fr.o80.beeryoubeerme.domain.model.Stock;

/**
 * Adapter for stocks list.
 *
 * @author Olivier Perez
 */
public class StockAdapter extends RecyclerView.Adapter<StockAdapter.StockViewHolder> {

    private final ListBeersListener mListener;
    private final List<Stock> mStocks;
    private final DateFormat mDateInstance;

    public StockAdapter(@NonNull final ListBeersListener listener, @NonNull final List<Stock> stocks) {
        mListener = listener;
        mStocks = stocks;
        mDateInstance = SimpleDateFormat.getDateInstance(DateFormat.SHORT);
    }

    @Override
    public StockViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stock, parent, false);
        return new StockViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StockViewHolder holder, int position) {
        Stock stock = mStocks.get(position);
        holder.bind(stock);
        holder.listen(stock);
    }

    @Override
    public int getItemCount() {
        return mStocks.size();
    }

    public interface ListBeersListener {
        void onEditStock(Stock stock);
    }

    final class StockViewHolder extends RecyclerView.ViewHolder {

        private static final int COLLAPSE_DURATION = 500;

        private boolean mCollapse = true;
        private boolean mNoBrewer = false;
        private boolean mNoComment = false;

        public final View mItemView;

        @Bind (R.id.stock)
        public RelativeLayout view;

        @Bind (R.id.beerName)
        public TextView mBeerName;

        @Bind (R.id.quantity)
        public TextView mQuantity;

        @Bind (R.id.expiry)
        public TextView mExpiry;

        @Bind (R.id.brewer)
        public TextView mBrewer;

        @Bind (R.id.rate)
        public TextView mRate;

        @Bind (R.id.comment)
        public TextView mComment;

        @Bind (R.id.volume)
        public TextView mVolume;

        @Bind (R.id.editBtn)
        public ImageButton mEditBtn;

        public StockViewHolder(final View itemView) {
            super(itemView);
            mItemView = itemView;
            mItemView.setTag(this);

            ButterKnife.bind(this, itemView);
            applyCollapse(0);

            view.setOnClickListener(v -> {
                mCollapse = !mCollapse;
                applyCollapse(COLLAPSE_DURATION);
            });
        }

        private void applyCollapse(int duration) {
            if (mCollapse) {
                ValueAnimator animation = ValueAnimator.ofFloat(1f, 0f);
                animation
                      .addUpdateListener(animation1 -> {
                          Float animatedValue = (Float) animation1.getAnimatedValue();
                          mRate.setAlpha(animatedValue);
                          mEditBtn.setAlpha(animatedValue);
                          mBrewer.setAlpha(animatedValue);
                          mComment.setAlpha(animatedValue);
                          if (animatedValue == 0) {
                              mRate.setVisibility(View.GONE);
                              mEditBtn.setVisibility(View.GONE);
                              mBrewer.setVisibility(View.GONE);
                              mComment.setVisibility(View.GONE);
                          }
                      });
                animation
                      .setDuration(duration)
                      .start();
            } else {
                mRate.setVisibility(View.VISIBLE);
                mEditBtn.setVisibility(View.VISIBLE);
                mBrewer.setVisibility(mNoBrewer ? View.GONE : View.VISIBLE);
                mComment.setVisibility(mNoComment ? View.GONE : View.VISIBLE);
                ValueAnimator animation = ValueAnimator.ofFloat(0f, 1f);
                animation
                      .addUpdateListener(animation1 -> {
                          mRate.setAlpha((Float) animation1.getAnimatedValue());
                          mEditBtn.setAlpha((Float) animation1.getAnimatedValue());
                          mBrewer.setAlpha((Float) animation1.getAnimatedValue());
                          mComment.setAlpha((Float) animation1.getAnimatedValue());
                      });
                animation
                      .setDuration(duration)
                      .start();
            }
        }

        public void noBrewer() {
            mNoBrewer = true;
        }

        public void noComment() {
            mNoComment = true;
        }

        public void bind(final Stock stock) {
            mBeerName.setText(stock.getBeer().getName());
            mQuantity.setText(MessageFormat.format("({0})", stock.getQuantity()));
            mExpiry.setText(mDateInstance.format(stock.getExpiry()));
            mRate.setText(MessageFormat.format("{0} / 10", stock.getBeer().getRate()));
            mVolume.setText(MessageFormat.format("{0}cl", stock.getVolume()));

            if (stock.getBeer().getBrewer() == null || stock.getBeer().getBrewer().isEmpty()) {
                noBrewer();
            } else {
                mBrewer.setText(stock.getBeer().getBrewer());
            }

            if (stock.getBeer().getComment() == null || stock.getBeer().getComment().isEmpty()) {
                noComment();
            } else {
                mComment.setText(stock.getBeer().getComment());
            }
        }

        public void listen(final Stock stock) {
            mEditBtn.setOnClickListener(view -> mListener.onEditStock(stock));
        }
    }
}
