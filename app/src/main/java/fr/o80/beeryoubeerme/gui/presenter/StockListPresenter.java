package fr.o80.beeryoubeerme.gui.presenter;

import android.util.Log;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import fr.o80.beeryoubeerme.BeerApplication;
import fr.o80.beeryoubeerme.domain.database.BeerDatabase;
import fr.o80.beeryoubeerme.domain.model.Stock;
import fr.o80.beeryoubeerme.domain.model.StockComparator;
import fr.o80.beeryoubeerme.gui.adapter.StockAdapter;
import fr.o80.beeryoubeerme.gui.fragment.StockListFragment;

/**
 * @author Olivier Perez
 */
public class StockListPresenter extends Presenter<StockListFragment> implements StockAdapter.ListBeersListener {

    @Inject
    BeerDatabase database;

    @Inject
    StockComparator mStockComparator;

    public StockListPresenter() {
        BeerApplication.component().inject(this);
        Log.d("","");
    }

    public void onFabClicked() {
        // TODO Ouvrir le fragment d'edition/d'ajout de stock : getView().addFragment(new LeDitFragment());
        getView().addFragment(null);
    }

    @Override
    public void init() {
        // TODO Extraire dans la couche domain + Utiliser RxJava
        try {
            List<Stock> items = database.getStockDao().queryForAll();
            Collections.sort(items, mStockComparator);
            getView().showStocks(items);
        } catch (SQLException e) {
            // TODO
            e.printStackTrace();
        }
    }

    @Override
    public void onEditStock(final Stock stock) {

    }
}
