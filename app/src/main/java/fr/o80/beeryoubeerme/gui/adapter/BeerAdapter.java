package fr.o80.beeryoubeerme.gui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.o80.beeryoubeerme.BeerApplication;
import fr.o80.beeryoubeerme.R;
import fr.o80.beeryoubeerme.domain.model.Beer;
import fr.o80.beeryoubeerme.domain.model.BeerColor;
import fr.o80.beeryoubeerme.gui.view.StatedImageView;

/**
 * Adapter for beer list.
 *
 * @author Olivier Perez
 */
public class BeerAdapter extends RecyclerView.Adapter<BeerAdapter.BeerViewHolder> {

    private BeerListListener mListener;
    private final List<Beer> mBeers;

    public BeerAdapter(@NonNull BeerListListener listener, @NonNull final List<Beer> beers) {
        mListener = listener;
        mBeers = beers;
    }

    @Override
    public BeerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_beer, parent, false);
        return new BeerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BeerViewHolder holder, final int position) {
        Beer beer = mBeers.get(position);

        holder.bind(beer);
        holder.listen(beer);
    }

    @Override
    public int getItemCount() {
        return mBeers.size();
    }

    public interface BeerListListener {

        void addBeerToCart(final int adapterPosition, Beer beer);

    }

    final class BeerViewHolder extends RecyclerView.ViewHolder {

        public final View mItemView;

        @Bind (R.id.beerName)
        TextView beerName;

        @Bind (R.id.cartView)
        StatedImageView toggleCart;

        public BeerViewHolder(final View itemView) {
            super(itemView);
            mItemView = itemView;

            ButterKnife.bind(this, itemView);

            toggleCart.addState(0, R.drawable.ic_shopping_cart_white_48dp);
            toggleCart.addState(1, R.drawable.ic_shopping_cart_black_48dp);
            toggleCart.setState(0);
        }

        public void bind(final Beer beer) {
            BeerColor beerColor = BeerColor.fromArray(beer.getColor(), BeerApplication.context().getResources().getStringArray(R.array.beer_colors));

            beerName.setText(beer.getName());
            if (beerColor != null) {
                beerName.setTextColor(beerColor.getTextColor());
                itemView.setBackgroundColor(beerColor.getBackgroundColor());
            } else {
                itemView.setBackgroundResource(R.drawable.cardview_background);
            }

            toggleCart.setState(beer.isToBuy() ? 1 : 0);
        }

        public void listen(final Beer beer) {
            toggleCart.setOnClickListener(v -> {
                mListener.addBeerToCart(getAdapterPosition(), beer);
                toggleCart.setState(beer.isToBuy() ? 1 : 0);
            });
        }
    }

}
