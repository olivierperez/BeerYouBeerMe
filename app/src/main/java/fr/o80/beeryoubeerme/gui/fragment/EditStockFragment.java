package fr.o80.beeryoubeerme.gui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Spinner;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.parceler.Parcels;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.o80.beeryoubeerme.BeerApplication;
import fr.o80.beeryoubeerme.R;
import fr.o80.beeryoubeerme.domain.database.BeerDatabase;
import fr.o80.beeryoubeerme.domain.model.Beer;
import fr.o80.beeryoubeerme.domain.model.BeerColor;
import fr.o80.beeryoubeerme.domain.model.Stock;


/**
 * The fragment used to add a new Stock, or to edit one.
 */
public class EditStockFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private static final String EXTRA_STOCK = "EXTRA_STOCK";

    @Inject
    BeerDatabase mBeerDatabase;

    @Bind (R.id.beer)
    Spinner mBeer;

    @Bind (R.id.beerName)
    EditText mBeerName;

    @Bind (R.id.brewer)
    EditText mBrewer;

    @Bind (R.id.color)
    MaterialBetterSpinner mColor;

    @Bind (R.id.type)
    MaterialBetterSpinner mType;

    @Bind (R.id.rate)
    EditText mRate;

    @Bind (R.id.comment)
    EditText mComment;

    @Bind (R.id.quantity)
    EditText mQuantity;

    @Bind (R.id.expiry)
    EditText mExpiry;

    @Bind (R.id.volume)
    EditText mVolume;

    @Bind (R.id.submit)
    Button mSubmit;

    private EditStockFragmentListener mListener;

    private Stock mStock;
    private Beer mSelectedBeer;

    private DateFormat mDateInstance;
    private List<Beer> mBeers;
    private boolean mEditing;

    public EditStockFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static EditStockFragment buildEditFragment(@NonNull EditStockFragmentListener listener, Stock stock) {
        EditStockFragment fragment = new EditStockFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_STOCK, Parcels.wrap(stock));
        fragment.setArguments(args);
        fragment.mListener = listener;
        return fragment;
    }

    @NonNull
    public static EditStockFragment buildAddFragment(@NonNull EditStockFragmentListener listener) {
        EditStockFragment fragment = new EditStockFragment();
        fragment.mListener = listener;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BeerApplication.component().inject(this);
        mDateInstance = SimpleDateFormat.getDateInstance(DateFormat.SHORT);
        if (getArguments() != null) {
            mStock = Parcels.unwrap(getArguments().getParcelable(EXTRA_STOCK));
            mEditing = true;
        } else {
            mStock = new Stock();
            mEditing = false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_beer, container, false);
        ButterKnife.bind(this, view);

        mVolume.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                saveBeer();
                return true;
            }
            return false;
        });

        mSubmit.setOnClickListener(v -> saveBeer());

        // TODO Use saveInstance
        try {
            // Beers
            Beer beer = new Beer();
            beer.setName(getString(R.string.new_beer));
            mBeers = new ArrayList<>();
            mBeers.add(beer);
            mBeers.addAll(mBeerDatabase.getBeersDao().queryForAll());

            mBeer.setAdapter(new BeerAdapter(getActivity(), mBeers));
            mBeer.setOnItemSelectedListener(this);

            // Colors
            String[] colors = getResources().getStringArray(R.array.beer_colors);
            mColor.setAdapter(new ColorAdapter(getActivity(), colors));
            mColor.setOnItemSelectedListener(this);

            // Types
            String[] types = getResources().getStringArray(R.array.beer_types);
            mType.setAdapter(new TypeAdapter(getActivity(), types));
            mType.setOnItemSelectedListener(this);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Set fields if we're editing a stock
        if (mEditing) {
            bindFields();
        }

        return view;
    }

    private boolean checkForm() {
        boolean checked = true;

        // Beer name
        if (mSelectedBeer == null && mBeerName.getText().toString().isEmpty()) {
            mBeerName.setError(BeerApplication.context().getString(R.string.error_beername_empty));
            checked = false;
        }

        // Rate
        if ((mEditing || mSelectedBeer == null) && mRate.getText().toString().isEmpty()) {
            mRate.setError(BeerApplication.context().getString(R.string.error_rate_empty));
            checked = false;
        }

        // Quantity
        if (mQuantity.getText().toString().isEmpty()) {
            mQuantity.setError(BeerApplication.context().getString(R.string.error_quantity_empty));
            checked = false;
        }

        // Expiry date
        if (mExpiry.getText().toString().isEmpty()) {
            mExpiry.setError(BeerApplication.context().getString(R.string.error_expiry_not_date));
            checked = false;
        } else {
            try {
                mDateInstance.parse(mExpiry.getText().toString());
            } catch (ParseException e) {
                mExpiry.setError(BeerApplication.context().getString(R.string.error_expiry_not_date));
                checked = false;
            }
        }

        // Volume
        if (mVolume.getText().toString().isEmpty()) {
            mVolume.setError(BeerApplication.context().getString(R.string.error_volume_empty));
            checked = false;
        }

        return checked;
    }

    private void bindFields() {
        mBeer.setVisibility(View.GONE);
        mBeerName.setEnabled(false);
        mBeerName.setText(mStock.getBeer().getName());
        mBrewer.setText(mStock.getBeer().getBrewer());
        mType.setText(mStock.getBeer().getType());
        mColor.setText(mStock.getBeer().getColor());
        mRate.setText(String.valueOf(mStock.getBeer().getRate()));
        mComment.setText(mStock.getBeer().getComment());
        mQuantity.setText(String.valueOf(mStock.getQuantity()));
        mExpiry.setText(mDateInstance.format(mStock.getExpiry()));
        mVolume.setText(String.valueOf(mStock.getVolume()));
        mSubmit.setText(R.string.validate);
    }

    private void saveBeer() {
        if (checkForm()) {
            doReallySave();
        }
    }

    private void doReallySave() {
        try {
            // Save Beer if needed
            Dao<Beer, Integer> beersDao = mBeerDatabase.getBeersDao();

            if (mEditing) {
                mStock.getBeer().setBrewer(mBrewer.getText().toString());
                mStock.getBeer().setColor(mColor.getText().toString());
                mStock.getBeer().setType(mType.getText().toString());
                mStock.getBeer().setRate(Integer.valueOf(mRate.getText().toString()));
                mStock.getBeer().setComment(mComment.getText().toString());

                beersDao.update(mStock.getBeer());
            } else {
                if (mSelectedBeer == null) {
                    Beer beer = new Beer();
                    beer.setName(mBeerName.getText().toString());
                    beer.setBrewer(mBrewer.getText().toString());
                    beer.setColor(mColor.getText().toString());
                    beer.setType(mType.getText().toString());
                    beer.setRate(Integer.valueOf(mRate.getText().toString()));
                    beer.setComment(mComment.getText().toString());
                    mStock.setBeer(beer);
                    beersDao.create(mStock.getBeer());
                } else {
                    mStock.setBeer(mSelectedBeer);
                }
            }

            // Save Stock
            Dao<Stock, Integer> dao = mBeerDatabase.getStockDao();

            mStock.setQuantity(Integer.valueOf(mQuantity.getText().toString()));
            mStock.setExpiry(mDateInstance.parse(mExpiry.getText().toString()));
            mStock.setVolume(Integer.valueOf(mVolume.getText().toString()));

            if (mEditing) {
                dao.update(mStock);
                mListener.onStockUpdated(mStock);
            } else {
                dao.create(mStock);
                mListener.onStockAdded(mStock);
            }


            // Leave fragment
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
        if (view == mBeer) {
            onBeerChanged(position);
        }
    }

    private void onBeerChanged(final int position) {
        if (mBeers.get(position).getId() == 0) {
            mSelectedBeer = null;
            mBeerName.setVisibility(View.VISIBLE);
            mBrewer.setVisibility(View.VISIBLE);
            mColor.setVisibility(View.VISIBLE);
            mRate.setVisibility(View.VISIBLE);
            mComment.setVisibility(View.VISIBLE);
        } else {
            mSelectedBeer = mBeers.get(position);
            mBeerName.setVisibility(View.GONE);
            mBrewer.setVisibility(View.GONE);
            mColor.setVisibility(View.GONE);
            mRate.setVisibility(View.GONE);
            mComment.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(final AdapterView<?> parent) {

    }

    public interface EditStockFragmentListener {

        /**
         * Triggered when a stock is updated
         */
        void onStockUpdated(Stock stock);

        /**
         * Triggered when a stock is created
         */
        void onStockAdded(Stock stock);
    }

    static class BeerAdapter extends BaseAdapter {

        private final Activity mActivity;
        private List<Beer> mBeers;

        @Bind (R.id.beerName)
        TextView beerName;

        public BeerAdapter(@NonNull Activity activity, @NonNull List<Beer> beers) {
            mActivity = activity;
            mBeers = beers;
        }

        @Override
        public int getCount() {
            return mBeers.size();
        }

        @Override
        public Object getItem(int position) {
            return mBeers.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mBeers.get(position).getId();
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = mActivity.getLayoutInflater().inflate(R.layout.spinner_beer, parent, false);
            } else {
                view = convertView;
            }
            // TODO Use a ViewHolder

            ButterKnife.bind(this, view);
            beerName.setText(mBeers.get(position).getName());

            return view;
        }
    }

    static class ColorAdapter extends BaseAdapter implements Filterable {

        private final String[] mColors;

        private final Activity mActivity;

        private Filter mFilter;

        @Bind (R.id.colorName)
        TextView mColorName;

        public ColorAdapter(@NonNull Activity activity, @NonNull String[] colors) {
            mActivity = activity;
            mColors = colors;
        }

        @Override
        public int getCount() {
            return mColors.length;
        }

        @Override
        public Object getItem(int position) {
            return mColors[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = mActivity.getLayoutInflater().inflate(R.layout.spinner_color, parent, false);
            } else {
                view = convertView;
            }
            // TODO Use a ViewHolder

            ButterKnife.bind(this, view);
            BeerColor beerColor = BeerColor.fromPosition(position);

            mColorName.setText(mColors[position]);
            mColorName.setTextColor(beerColor.getTextColor());
            view.setBackgroundColor(beerColor.getBackgroundColor());

            return view;
        }

        @Override
        public Filter getFilter() {
            if (mFilter == null) {
                mFilter = new Filter() {
                    @Override
                    protected FilterResults performFiltering(final CharSequence constraint) {
                        FilterResults results = new FilterResults();
                        results.values = mColors;
                        results.count = mColors.length;
                        return results;
                    }

                    @Override
                    protected void publishResults(final CharSequence constraint, final FilterResults results) {
                        if (results.count > 0) {
                            notifyDataSetChanged();
                        } else {
                            notifyDataSetInvalidated();
                        }
                    }
                };
            }

            return mFilter;
        }
    }

    static class TypeAdapter extends BaseAdapter implements Filterable {

        private final String[] mTypes;

        private final Activity mActivity;

        private Filter mFilter;

        @Bind (R.id.typeName)
        TextView mTypeName;

        public TypeAdapter(@NonNull Activity activity, @NonNull String[] types) {
            mActivity = activity;
            mTypes = types;
        }

        @Override
        public int getCount() {
            return mTypes.length;
        }

        @Override
        public Object getItem(int position) {
            return mTypes[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = mActivity.getLayoutInflater().inflate(R.layout.spinner_type, parent, false);
            } else {
                view = convertView;
            }
            // TODO Use a ViewHolder

            ButterKnife.bind(this, view);

            mTypeName.setText(mTypes[position]);

            return view;
        }

        @Override
        public Filter getFilter() {
            if (mFilter == null) {
                mFilter = new Filter() {
                    @Override
                    protected FilterResults performFiltering(final CharSequence constraint) {
                        FilterResults results = new FilterResults();
                        results.values = mTypes;
                        results.count = mTypes.length;
                        return results;
                    }

                    @Override
                    protected void publishResults(final CharSequence constraint, final FilterResults results) {
                        if (results.count > 0) {
                            notifyDataSetChanged();
                        } else {
                            notifyDataSetInvalidated();
                        }
                    }
                };
            }

            return mFilter;
        }
    }

}
