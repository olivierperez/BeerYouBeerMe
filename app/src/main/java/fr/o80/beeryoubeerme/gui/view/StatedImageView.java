package fr.o80.beeryoubeerme.gui.view;

/**
 * @author Olivier Perez
 */

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO
 * @author Olivier Perez
 */
public class StatedImageView extends ImageView {

    public static final int FADE_DURATION = 150;
    private Map<Integer, Integer> states = new HashMap<>();

    public StatedImageView(final Context context) {
        this(context, null);
    }

    public StatedImageView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StatedImageView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi (Build.VERSION_CODES.LOLLIPOP)
    public StatedImageView(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * TODO
     * @param state
     * @param res
     */
    public void addState(final int state, @DrawableRes final int res) {
        states.put(state, res);
    }

    /**
     * TODO
     * @param state
     */
    public void setState(final int state) {
        Drawable currentDrawable = getDrawable();
        Drawable newDrawable = getResources().getDrawable(states.get(state));

        if (currentDrawable == null) {
            setImageDrawable(newDrawable);
        } else {
            TransitionDrawable crossfader = new TransitionDrawable(
                  new Drawable[] {
                        currentDrawable,
                        newDrawable
                  }
            );
            setImageDrawable(crossfader);

            crossfader.setCrossFadeEnabled(true);
            crossfader.startTransition(FADE_DURATION);
        }
    }
}
