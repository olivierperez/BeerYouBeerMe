package fr.o80.beeryoubeerme.gui.fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import fr.o80.beeryoubeerme.BeerApplication;
import fr.o80.beeryoubeerme.R;
import fr.o80.beeryoubeerme.gui.adapter.BeerAdapter;
import fr.o80.beeryoubeerme.domain.database.BeerDatabase;
import fr.o80.beeryoubeerme.domain.model.Beer;

/**
 * @author Olivier Perez
 */
public class ListBeerFragment extends RecyclerViewFragment<Beer> implements BeerAdapter.BeerListListener {

    @Inject
    BeerDatabase database;

    public static ListBeerFragment newInstance() {
        return new ListBeerFragment();
    }

    public ListBeerFragment() {
        super(R.string.there_are_no_references, false);
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BeerApplication.component().inject(this);
    }

    @Override
    protected RecyclerView.Adapter<? extends RecyclerView.ViewHolder> newAdapter() {
        return new BeerAdapter(this, mItems);
    }

    @Override
    protected void onFabClicked() {
        // There is no FAB
    }

    @Override
    protected List<Beer> doLoadItems() {
        try {
            return database.getBeersDao().queryForAll();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void sortItems(final List<Beer> items) {

    }

    @Override
    public void addBeerToCart(final int adapterPosition, final Beer beer) {
        beer.setToBuy(!beer.isToBuy());
        try {
            database.getBeersDao().update(beer);
        } catch (SQLException e) {
            beer.setToBuy(!beer.isToBuy());
            // TODO Afficher une SnackBar
        }
    }
}
