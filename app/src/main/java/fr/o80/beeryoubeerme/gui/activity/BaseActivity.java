package fr.o80.beeryoubeerme.gui.activity;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import fr.o80.beeryoubeerme.R;

/**
 * @author Olivier Perez
 */
public class BaseActivity extends AppCompatActivity {

    public void addFragment(Fragment fragment) {
        getSupportFragmentManager()
              .beginTransaction()
              .add(R.id.main_content, fragment, null)
              .commit();
    }

}
