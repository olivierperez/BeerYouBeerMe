package fr.o80.beeryoubeerme.gui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.o80.beeryoubeerme.R;
import fr.o80.beeryoubeerme.domain.model.Stock;
import fr.o80.beeryoubeerme.gui.adapter.StockAdapter;
import fr.o80.beeryoubeerme.gui.presenter.StockListPresenter;

/**
 * The list of beers.
 *
 * @author Olivier Perez
 */
public class StockListFragment extends BaseFragment {

    @Bind (R.id.recyclerView)
    RecyclerView mBeersView;

    @Bind (R.id.emptyText)
    TextView mEmptyText;

    private StockListPresenter mPresenter;

    protected List<Stock> mItems;

    private StockAdapter mAdapter;

    public StockListFragment() {}

    public static StockListFragment newInstance() {
        return new StockListFragment();
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new StockListPresenter();
        mPresenter.setView(this);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        ButterKnife.bind(this, view);

        mItems = new ArrayList<>();
        mAdapter = new StockAdapter(mPresenter, mItems);

        mBeersView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBeersView.setItemAnimator(new DefaultItemAnimator());
        mBeersView.setAdapter(mAdapter);

        mEmptyText.setText(R.string.you_have_no_beers);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener((v) -> mPresenter.onFabClicked());

        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.init();
    }

    public void showStocks(final List<Stock> items) {
        // Show stocks
        mItems.clear();
        mItems.addAll(items);
        mAdapter.notifyDataSetChanged();

        // Hide empty-text
        mEmptyText.setVisibility(View.GONE);
    }
}
