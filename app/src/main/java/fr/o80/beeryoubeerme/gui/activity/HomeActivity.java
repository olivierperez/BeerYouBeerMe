package fr.o80.beeryoubeerme.gui.activity;

import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;

import butterknife.ButterKnife;
import fr.o80.beeryoubeerme.R;
import fr.o80.beeryoubeerme.gui.fragment.StockListFragment;

public class HomeActivity extends BaseActivity {

    public static final int REQUEST_CODE_ADD_STOCK = 0x0001;
    public static final int REQUEST_CODE_UPDATE_STOCK = 0x0002;
//    private MyPagerAdapater mPagerAdapater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_drawer);
        ButterKnife.bind(this);

        // Toolbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Drawer

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
              this, drawer, toolbar, R.string.menu_open, R.string.menu_close);
        toggle.syncState();

        // Add first Fragment

        addFragment(StockListFragment.newInstance());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
