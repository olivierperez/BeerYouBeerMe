package fr.o80.beeryoubeerme.gui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import org.parceler.Parcels;

import fr.o80.beeryoubeerme.BeerApplication;
import fr.o80.beeryoubeerme.R;
import fr.o80.beeryoubeerme.gui.fragment.EditStockFragment;
import fr.o80.beeryoubeerme.domain.model.Stock;

/**
 * @author Olivier Perez
 */
public class StockActivity extends AppCompatActivity implements EditStockFragment.EditStockFragmentListener {

    public static final String EXTRA_STOCK = "STOCK";

    public static Intent editStockIntent(@NonNull Stock stock) {
        Intent intent = new Intent(BeerApplication.context(), StockActivity.class);
        intent.putExtra(EXTRA_STOCK, Parcels.wrap(stock));
        return intent;
    }

    public static Intent newStockIntent() {
        return new Intent(BeerApplication.context(), StockActivity.class);
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sotck);


        Fragment previousFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
        if (previousFragment == null) {
            Bundle extras = getIntent().getExtras();

            if (extras != null && extras.containsKey(EXTRA_STOCK)) { // Load edit fragment
                Stock stock = Parcels.unwrap(extras.getParcelable(EXTRA_STOCK));
                getSupportFragmentManager()
                      .beginTransaction()
                      .add(R.id.frame_layout, EditStockFragment.buildEditFragment(this, stock))
                      .commit();

            } else { // Load add fragment
                getSupportFragmentManager()
                      .beginTransaction()
                      .add(R.id.frame_layout, EditStockFragment.buildAddFragment(this))
                      .commit();

            }
        }
    }

    @Override
    public void onStockAdded(final Stock stock) {
        Intent data = new Intent();
        data.putExtra(EXTRA_STOCK, Parcels.wrap(stock));
        setResult(HomeActivity.REQUEST_CODE_ADD_STOCK, data);
        super.onBackPressed();
    }

    @Override
    public void onStockUpdated(final Stock stock) {
        Intent data = new Intent();
        data.putExtra(EXTRA_STOCK, Parcels.wrap(stock));
        setResult(HomeActivity.REQUEST_CODE_UPDATE_STOCK, data);
        super.onBackPressed();
    }
}
