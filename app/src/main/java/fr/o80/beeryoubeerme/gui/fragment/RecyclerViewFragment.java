package fr.o80.beeryoubeerme.gui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.o80.beeryoubeerme.R;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Olivier Perez
 */
public abstract class RecyclerViewFragment<T> extends Fragment {

    public static final String ITEMS = "ITEMS";

    private final int mEmptyTextRes;
    private final boolean mDisplayFab;

    protected List<T> mItems;

    private RecyclerView.Adapter<? extends RecyclerView.ViewHolder> mAdapter;

    private Subscription mSubscription;

    @Bind (R.id.recyclerView)
    RecyclerView mBeersView;

    @Bind (R.id.emptyText)
    TextView mEmptyText;

    protected RecyclerViewFragment(@StringRes int emptyTextRes, boolean displayFab) {
        mItems = new ArrayList<>();
        mEmptyTextRes = emptyTextRes;
        mDisplayFab = displayFab;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null && savedInstanceState.containsKey(ITEMS)) {
            mItems = Parcels.unwrap(savedInstanceState.getParcelable(ITEMS));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recyclerview, container, false);

        ButterKnife.bind(this, view);
        mAdapter = newAdapter();

        mBeersView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBeersView.setItemAnimator(new DefaultItemAnimator());
        mBeersView.setAdapter(mAdapter);

        mEmptyText.setText(mEmptyTextRes);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        if (mDisplayFab) {
            fab.setOnClickListener((v) -> this.onFabClicked());
        } else {
            fab.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mItems.size() == 0) {
            loadItems();
        } else {
            mEmptyText.setVisibility(View.GONE);
        }
    }

    private void loadItems() {
        mSubscription = Observable
              .defer(() -> Observable.just(this.doLoadItems()))
              .subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe(items -> {
                  sortItems(items);
                  mEmptyText.setVisibility(items.size() > 0 ? View.GONE : View.VISIBLE);
                  mItems.clear();
                  mItems.addAll(items);
                  mAdapter.notifyDataSetChanged();
              });
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ITEMS, Parcels.wrap(mItems));
    }

    @Override
    public void onDestroyView() {
        super.onDestroy();
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    @NonNull
    protected RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter() {
        return mAdapter;
    }

    protected abstract RecyclerView.Adapter<? extends RecyclerView.ViewHolder> newAdapter();

    protected abstract void onFabClicked();

    protected abstract List<T> doLoadItems();

    protected abstract void sortItems(final List<T> items);
}
