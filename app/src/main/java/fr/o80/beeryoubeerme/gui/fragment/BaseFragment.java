package fr.o80.beeryoubeerme.gui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.o80.beeryoubeerme.gui.activity.BaseActivity;
import fr.o80.beeryoubeerme.gui.presenter.Presenter;

/**
 * @author Olivier Perez
 */
public abstract class BaseFragment extends Fragment implements Presenter.View {

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void addFragment(final BaseFragment fragment) {
        ((BaseActivity)getActivity()).addFragment(fragment);
    }
}
