package fr.o80.beeryoubeerme.gui.presenter;

import fr.o80.beeryoubeerme.gui.fragment.BaseFragment;

/**
 * @author Olivier Perez
 */
public abstract class Presenter<T extends Presenter.View>{

    private T view;

    public Presenter() {
    }

    public abstract void init();

    public T getView() {
        return view;
    }

    public void setView(final T view) {
        this.view = view;
    }

    public interface View {
        void addFragment(BaseFragment fragment);
    }
}
