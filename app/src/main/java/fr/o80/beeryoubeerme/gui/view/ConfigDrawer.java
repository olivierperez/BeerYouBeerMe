package fr.o80.beeryoubeerme.gui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import javax.inject.Inject;

import fr.o80.beeryoubeerme.BeerApplication;
import fr.o80.beeryoubeerme.R;
import fr.o80.beeryoubeerme.event.ChangeSortConfig;
import fr.o80.beeryoubeerme.service.SharedPref;

public class ConfigDrawer extends LinearLayout {

    @Inject
    SharedPref mSharedPref;

    public ConfigDrawer(final Context context) {
        this(context, null, 0);
    }

    public ConfigDrawer(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ConfigDrawer(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        BeerApplication.component().inject(this);

        View view = LayoutInflater.from(context).inflate(R.layout.config_drawer, this, true);

        initSpinner(context, view, R.id.config_drawer_spinner1, SharedPref.PREF_SORT_1);
        initSpinner(context, view, R.id.config_drawer_spinner2, SharedPref.PREF_SORT_2);
        initSpinner(context, view, R.id.config_drawer_spinner3, SharedPref.PREF_SORT_3);
    }

    private void initSpinner(final Context context, final View view, final int spinnerId, final String prefKey) {
        Spinner spinner = (Spinner) view.findViewById(spinnerId);

        // Pre-set the value from SharedPref
        String[] sortFields = context.getResources().getStringArray(R.array.sort_fields);
        String selectedValue = mSharedPref.getString(prefKey, "-");
        for (int i = 0 ; i < sortFields.length ; i++) {
            if (sortFields[i].equals(selectedValue)) {
                spinner.setSelection(i);
            }
        }

        // Handle modifications
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                String value = (String) parent.getItemAtPosition(position);
                mSharedPref.putString(prefKey, value);
//                bus.post(new ChangeSortConfig());
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {
                mSharedPref.putString(prefKey, "-");
//                bus.post(new ChangeSortConfig());
            }
        });
    }
}
