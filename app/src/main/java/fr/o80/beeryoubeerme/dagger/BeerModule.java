package fr.o80.beeryoubeerme.dagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fr.o80.beeryoubeerme.BeerApplication;
import fr.o80.beeryoubeerme.domain.database.BeerDatabase;

@Module
public class BeerModule {

    private BeerApplication mApplication;

    public BeerModule(final BeerApplication application) {
        mApplication = application;
    }

    @Provides
    public BeerApplication provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    public BeerDatabase provideDatabse(BeerApplication application) {
        return new BeerDatabase(application);
    }
}
