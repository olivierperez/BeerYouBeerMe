package fr.o80.beeryoubeerme.dagger;

import javax.inject.Singleton;

import dagger.Component;
import fr.o80.beeryoubeerme.gui.fragment.EditStockFragment;
import fr.o80.beeryoubeerme.gui.fragment.ListBeerFragment;
import fr.o80.beeryoubeerme.gui.fragment.StockListFragment;
import fr.o80.beeryoubeerme.gui.presenter.StockListPresenter;
import fr.o80.beeryoubeerme.gui.view.ConfigDrawer;

@Component (modules = BeerModule.class)
@Singleton
public interface BeerComponent {
    void inject(EditStockFragment fragment);
    void inject(ListBeerFragment listBeerFragment);
    void inject(ConfigDrawer view);

    void inject(StockListPresenter stockListPresenter);
}
