package fr.o80.beeryoubeerme.domain.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.Date;

import fr.o80.beeryoubeerme.domain.model.Beer;
import fr.o80.beeryoubeerme.domain.model.Stock;


public class BeerDatabase extends OrmLiteSqliteOpenHelper {

    public static final String TAG = BeerDatabase.class.getSimpleName();
    public static final String BEER_DATABASE = "BEER_DATABASE";
    public static final int DATABASE_VERSION = 1;
    public static final int VOLUME_50_CL = 50;
    public static final int VOLUME_75_CL = 75;

    public BeerDatabase(final Context context) {
        super(context, BEER_DATABASE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase database, final ConnectionSource connectionSource) {
        Log.i(TAG, "Create database");

        try {
            TableUtils.createTable(connectionSource, Beer.class);
            TableUtils.createTable(connectionSource, Stock.class);

            Beer b1614 = new Beer("1664", 0);
            Beer leffe = new Beer("Leffe Ruby", 2);
            Beer heineken = new Beer("Heineken", 1);
            Beer tsingtao = new Beer("Tsingtao", 1);

            Dao<Beer, Integer> beerDao = getBeersDao();
            beerDao.create(b1614);
            beerDao.create(leffe);
            beerDao.create(heineken);
            beerDao.create(tsingtao);

            Dao<Stock, Integer> purchaseDao = getStockDao();
            purchaseDao.create(new Stock(leffe, 10, new Date(), VOLUME_50_CL));
            purchaseDao.create(new Stock(heineken, 6, new Date(), VOLUME_75_CL));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase database, final ConnectionSource connectionSource, final int oldVersion, final int newVersion) {

    }

    public Dao<Beer, Integer> getBeersDao() throws SQLException {
        return getDao(Beer.class);
    }

    public Dao<Stock, Integer> getStockDao() throws SQLException {
        return getDao(Stock.class);
    }
}
