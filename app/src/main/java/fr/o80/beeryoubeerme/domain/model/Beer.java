package fr.o80.beeryoubeerme.domain.model;

import com.j256.ormlite.field.DatabaseField;

import org.parceler.Parcel;

@Parcel
public class Beer {

    @DatabaseField (generatedId = true)
    int id;

    @DatabaseField
    String mName;

    @DatabaseField
    String mBrewer;

    @DatabaseField
    String mColor;

    @DatabaseField
    String mType;

    @DatabaseField
    int mRate;

    @DatabaseField
    String mComment;

    @DatabaseField
    boolean toBuy;

    public Beer() {
    }

    public Beer(final String name, final int rate) {
        this.mName = name;
        this.mRate = rate;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(final String name) {
        mName = name;
    }

    public String getBrewer() {
        return mBrewer;
    }

    public void setBrewer(final String brewer) {
        this.mBrewer = brewer;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(final String color) {
        mColor = color;
    }

    public String getType() {
        return mType;
    }

    public void setType(final String type) {
        this.mType = type;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(final String comment) {
        this.mComment = comment;
    }

    public int getRate() {
        return mRate;
    }

    public void setRate(final int rate) {
        mRate = rate;
    }

    public boolean isToBuy() {
        return toBuy;
    }

    public void setToBuy(final boolean toBuy) {
        this.toBuy = toBuy;
    }
}
