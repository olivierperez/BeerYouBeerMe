package fr.o80.beeryoubeerme.domain.model;

import android.graphics.Color;

/**
 * @author Olivier Perez
 */
public enum BeerColor {
    WHITE("#fae04d", "#000000"),
    YELLOW("#edb52c", "#000000"),
    AMBER("#e4871d", "#000000"),
    BROWN("#741205", "#ffffff"),
    BLACK("#1d0000", "#ffffff");

    private final String mBackgroundColor;
    private final String mTextColor;

    BeerColor(String backgroundColor, String textColor) {
        mBackgroundColor = backgroundColor;
        mTextColor = textColor;
    }

    public static BeerColor fromPosition(int position) {
        return values()[position];
    }

    public static BeerColor fromArray(final String name, final String[] names) {
        for (int i = 0; i<names.length; i++) {
            if (names[i].equals(name)) {
                return fromPosition(i);
            }
        }
        return null;
    }

    public int getBackgroundColor() {
        return Color.parseColor(mBackgroundColor);
    }

    public int getTextColor() {
        return Color.parseColor(mTextColor);
    }
}
