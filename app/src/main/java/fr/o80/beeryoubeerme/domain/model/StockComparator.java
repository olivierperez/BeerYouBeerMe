package fr.o80.beeryoubeerme.domain.model;

import java.util.Comparator;

import javax.inject.Inject;

import fr.o80.beeryoubeerme.service.SharedPref;

/**
 * This comparator helps to sort Stock elements with configuration defined by the user in SharedPref.
 * @author Olivier Perez
 */
public class StockComparator implements Comparator<Stock> {

    @Inject
    SharedPref sharedPref;

    private SortField mSortField1;
    private SortField mSortField2;
    private SortField mSortField3;

    @Inject
    public StockComparator() {
    }

    public void reload() {
        mSortField1 = SortField.fromString(sharedPref.getString(SharedPref.PREF_SORT_1, "-"));
        mSortField2 = SortField.fromString(sharedPref.getString(SharedPref.PREF_SORT_2, "-"));
        mSortField3 = SortField.fromString(sharedPref.getString(SharedPref.PREF_SORT_3, "-"));
    }

    @Override
    public int compare(final Stock lhs, final Stock rhs) {
        if (mSortField1 == null) {
            reload();
        }

        int compare1 = mSortField1.compare(lhs, rhs);
        if (compare1 != 0) {
            return compare1;
        }
        int compare2 = mSortField2.compare(lhs, rhs);
        if (compare2 != 0) {
            return compare2;
        }

        int compare3 = mSortField3.compare(lhs, rhs);
        if (compare3 != 0) {
            return compare3;
        }

        return SortField.NAME.compare(lhs, rhs);
    }

    enum SortField {
        NOTHING("-", (lhs, rhs) -> 0), // No sort
        NAME("Nom", (lhs, rhs) -> lhs.getBeer().getName().compareTo(rhs.getBeer().getName())), // Sort by name ASC
        EXPIRY("Péremption", (lhs, rhs) -> lhs.getExpiry().compareTo(rhs.getExpiry())), // Sort by expiry, nearest first
        RATE("Note", (lhs, rhs) -> lhs.getBeer().getRate() - rhs.getBeer().getRate()); // Sort by rate, better first

        private final String mValue;
        private final Comparator<Stock> mComparator;

        SortField(String value, Comparator<Stock> comparator) {
            mValue = value;
            mComparator = comparator;
        }

        public int compare(Stock lhs, Stock rhs) {
            return mComparator.compare(lhs, rhs);
        }

        static SortField fromString(String value) {
            if (value == null) {
                return NOTHING;
            }

            for (SortField x : values()) {
                if (value.equals(x.mValue)) {
                    return x;
                }
            }

            return NOTHING;
        }
    }
}
