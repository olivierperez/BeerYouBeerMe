package fr.o80.beeryoubeerme.domain.model;

import com.j256.ormlite.field.DatabaseField;

import org.parceler.Parcel;

import java.util.Date;

@Parcel
public class Stock {

    @DatabaseField (generatedId = true)
    int id;

    @DatabaseField (foreign = true, foreignAutoRefresh = true)
    Beer mBeer;

    @DatabaseField
    int mQuantity;

    @DatabaseField
    Date mExpiry;

    @DatabaseField
    int mVolume;

    public Stock() {
    }

    public Stock(final Beer beer, final int quantity, final Date expiry, final int volume) {
        mExpiry = expiry;
        mBeer = beer;
        mQuantity = quantity;
        mVolume = volume;
    }

    public int getId() {
        return id;
    }

    public Beer getBeer() {
        return mBeer;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public Date getExpiry() {
        return mExpiry;
    }

    public int getVolume() {
        return mVolume;
    }

    public void setBeer(final Beer beer) {
        mBeer = beer;
    }

    public void setQuantity(final int quantity) {
        mQuantity = quantity;
    }

    public void setExpiry(final Date expiry) {
        mExpiry = expiry;
    }

    public void setVolume(final int volume) {
        mVolume = volume;
    }
}
