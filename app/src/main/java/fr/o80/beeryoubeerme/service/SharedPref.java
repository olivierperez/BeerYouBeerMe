package fr.o80.beeryoubeerme.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import javax.inject.Inject;

import fr.o80.beeryoubeerme.BeerApplication;

/**
 * This class simplify access to SharedPreferences.
 * Note: Use generic key strings.
 *
 * @author Olivier Perez
 */
public class SharedPref {

    private static final String PREFS = "PREFS";

    public static final String PREF_SORT_1 = "pref_0_0";
    public static final String PREF_SORT_2 = "pref_0_1";
    public static final String PREF_SORT_3 = "pref_0_2";

    private final BeerApplication mApplication;

    @Inject
    public SharedPref(BeerApplication application) {
        mApplication = application;
    }

    public void putString(final String key, final String value) {
        sharedPreferences()
              .edit()
              .putString(key, value)
              .commit();
    }

    public String getString(final String key, final String defaultValue) {
        return sharedPreferences()
              .getString(key, defaultValue);
    }

    private SharedPreferences sharedPreferences() {
        return mApplication.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
    }
}
