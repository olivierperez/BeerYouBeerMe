package fr.o80.beeryoubeerme;

import android.app.Application;
import android.content.Context;

import fr.o80.beeryoubeerme.dagger.BeerComponent;
import fr.o80.beeryoubeerme.dagger.BeerModule;
import fr.o80.beeryoubeerme.dagger.DaggerBeerComponent;

public class BeerApplication extends Application {

    private BeerComponent mBeerComponent;
    private static BeerApplication mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mBeerComponent = DaggerBeerComponent.builder()
              .beerModule(new BeerModule(this))
              .build();
        mContext = this;
    }

    public static BeerComponent component() {
        return mContext.mBeerComponent;
    }

    public static Context context() {
        return mContext;
    }
}
